/**
 * @author sgb004
 */
 
jQuery(window).scroll(function() {
	var i;
	var wa = document.querySelectorAll('.wpb_animate_when_almost_visible:not([class*="wpb_start_animation"])');
	for( i=0; i<wa.length; i++ ){
		if( elementInViewport(wa[i]) ){
			wa[i].classList.add('wpb_start_animation');
			wa[i].classList.add('animated');
		}
	}
});

/**
 * @link https://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
 */
function elementInViewport(el) {
	var top = el.offsetTop;
	var left = el.offsetLeft;
	var width = el.offsetWidth;
	var height = el.offsetHeight;

	while(el.offsetParent) {
		el = el.offsetParent;
		top += el.offsetTop;
		left += el.offsetLeft;
	}

	return (
		top >= window.pageYOffset &&
		left >= window.pageXOffset &&
		(top + height) <= (window.pageYOffset + window.innerHeight) &&
		(left + width) <= (window.pageXOffset + window.innerWidth)
	);
}